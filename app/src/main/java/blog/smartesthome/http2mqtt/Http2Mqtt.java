package blog.smartesthome.http2mqtt;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

import java.net.URISyntaxException;

@SpringBootApplication
public class Http2Mqtt implements ApplicationListener<ContextClosedEvent> {


  public static void main(String[] args) throws URISyntaxException {
    SpringApplication.run(Http2Mqtt.class, args);
  }

  @Override
  public void onApplicationEvent(ContextClosedEvent event) {
    MqttClient client = event.getApplicationContext().getBean(MqttClient.class);
    try {
      client.disconnect();
    } catch (MqttException e) {
      e.printStackTrace();
    }
  }
}
