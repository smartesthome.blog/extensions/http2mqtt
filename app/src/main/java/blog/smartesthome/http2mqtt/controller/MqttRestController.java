package blog.smartesthome.http2mqtt.controller;

import blog.smartesthome.http2mqtt.model.HttpMqttRequest;
import blog.smartesthome.http2mqtt.model.HttpMqttResponse;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
public class MqttRestController {

  @Autowired
  private MqttClient client;

  @Async
  @PostMapping(path = "/publish", consumes = "application/json" , produces = "application/json")
  public ResponseEntity<HttpMqttResponse> publish(@RequestBody HttpMqttRequest request) {
    ResponseEntity<HttpMqttResponse> result;
    try {
      MqttMessage message = new MqttMessage(request.getMessage().getBytes("UTF-8"));
      message.setRetained(request.isRetained());
      client.publish(request.getTopic(), message);
      result = new ResponseEntity<>(new HttpMqttResponse("Ok"), HttpStatus.OK);
    } catch (MqttException | UnsupportedEncodingException e) {
      result = new ResponseEntity<>(new HttpMqttResponse("Error", e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }
    return result;

  }

}
