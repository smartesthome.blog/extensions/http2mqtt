package blog.smartesthome.http2mqtt.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:blog/smartesthome/http2mqtt/configuration.properties")
public class MqttConfiguration {

  @Value("${mqtt.broker:tcp://localhost:1883}")
  private String broker;
  @Value("${mqtt.clientId:http2mqtt-bridge}")
  private String clientId;
  @Value("${mqtt.qos:2}")
  private int qos;
  @Value("${mqtt.username:}")
  private String username;
  @Value("${mqtt.password:")
  private String password;
  @Value("${mqtt.keepAliveInterval:60}")
  private int keepAliveInterval;
  @Value("${mqtt.connectionTimeout:30}")
  private int connectionTimeout;

  public MqttConfiguration() {
  }

  public String getBroker() {
    return broker;
  }

  public void setBroker(String broker) {
    this.broker = broker;
  }

  public String getClientId() {
    return clientId;
  }

  public void setClientId(String clientId) {
    this.clientId = clientId;
  }

  public int getQos() {
    return qos;
  }

  public void setQos(int qos) {
    this.qos = qos;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public boolean hasCredentials() {
    return getUsername() != null && !getUsername().trim().isEmpty() && getPassword() != null && !getPassword().trim().isEmpty();
  }

  public int getKeepAliveInterval() {
    return keepAliveInterval;
  }

  public void setKeepAliveInterval(int keepAliveInterval) {
    this.keepAliveInterval = keepAliveInterval;
  }

  public int getConnectionTimeout() {
    return connectionTimeout;
  }

  public void setConnectionTimeout(int connectionTimeout) {
    this.connectionTimeout = connectionTimeout;
  }
}
