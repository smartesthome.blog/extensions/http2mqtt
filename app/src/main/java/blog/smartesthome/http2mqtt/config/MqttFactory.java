package blog.smartesthome.http2mqtt.config;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

@Configuration
public class MqttFactory {

  @Autowired
  private MqttConfiguration configuration;

  @Bean
  public Executor asyncExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(2);
    executor.setMaxPoolSize(4);
    executor.setQueueCapacity(500);
    executor.setThreadNamePrefix("Http2Mqtt");
    executor.initialize();
    return executor;
  }

  @Bean
  public MqttConnectOptions mqttConnectOptions() {
    MqttConnectOptions options = new MqttConnectOptions();
    options.setCleanSession(true);
    if( configuration.hasCredentials() ) {
      options.setUserName(configuration.getUsername());
      options.setPassword(configuration.getPassword().toCharArray());
    }
    options.setConnectionTimeout(configuration.getConnectionTimeout());
    options.setKeepAliveInterval(configuration.getKeepAliveInterval());
    return options;
  }

  @Bean
  public MqttClient mqttClient(MqttConnectOptions options) throws Exception {
    MqttClient client = new MqttClient(configuration.getBroker(), configuration.getClientId());
    client.connect(options);
    return client;
  }

}
