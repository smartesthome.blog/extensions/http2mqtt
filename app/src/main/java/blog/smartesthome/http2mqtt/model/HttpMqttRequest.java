package blog.smartesthome.http2mqtt.model;

public class HttpMqttRequest {

  private String topic;
  private String message;
  private boolean retained = false;

  public HttpMqttRequest() {
  }

  public String getTopic() {
    return topic;
  }

  public void setTopic(String topic) {
    this.topic = topic;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public boolean isRetained() {
    return retained;
  }

  public void setRetained(boolean retained) {
    this.retained = retained;
  }
}
