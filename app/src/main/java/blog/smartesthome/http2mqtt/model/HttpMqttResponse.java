package blog.smartesthome.http2mqtt.model;

public class HttpMqttResponse {

  private String status;
  private String message;

  public HttpMqttResponse() {
  }

  public HttpMqttResponse(String status) {
    this.status = status;
  }

  public HttpMqttResponse(String status, String message) {
    this.status = status;
    this.message = message;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
