http2mqtt
=========

Simple Http Web service mapping the HTTP request to a MQTT broker.
Publish to MQTT only!

## Properties
__mqtt.broker__ (Default: tcp://localhost:1883)

__mqtt.clientId__ (Default: http2mqtt-bridge)

__mqtt.qos__ (Default: 2)

__mqtt.username__ (Default: _empty_)

__mqtt.password__ (Default: _empty_)

__mqtt.keepAliveInterval__ (Default: 60)

__mqtt.connectionTimeout__ (Default: 30)

## Usage of properties
Pass properties as Java VM arguments to the program to overwrite defaults.

`-Dmqtt.broker=tcp://192.168.0.2:1883`

## Example execution
`java -Dmqtt.broker=tcp://192.168.0.2:1883 -jar http2mqtt.jar`

## Docker Image
`docker pull iulius/http2mqtt:latest`

### Docker Environment Variables
__MQTT_BROKER__ (Default: tcp://localhost:1883)

__MQTT_CLIENT__ (Default: http2mqtt-bridge)

__MQTT_QOS__ (Default: 2)

__MQTT_USERNAME__ (Default: _empty_)

__MQTT_PASSWORD__ (Default: _empty_)

__MQTT_KEEP_ALIVE_INTERVAL__ (Default: 60)

__MQTT_CONNECTION_TIMEOUT__ (Default: 30)


### Docker example run 

`docker run -e MQTT_BROKER=tcp://192.168.0.2:1883 -p 8080:8080 iulius/http2mqtt:latest`