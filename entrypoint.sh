#!/bin/sh

echo Broker: $MQTT_BROKER
echo Client Id: $MQTT_CLIENT
echo QoS: $MQTT_QOS
echo Username: $MQTT_USERNAME
echo Keep Alive Interval: $MQTT_KEEP_ALIVE_INTERVAL
echo Connection Timeout: $MQTT_CONNECTION_TIMEOUT

java -Djava.security.egd=file:/dev/./urandom \
     -Dmqtt.broker=$MQTT_BROKER \
     -Dmqtt.clientId=$MQTT_CLIENT \
     -Dmqtt.qos=$MQTT_QOS \
     -Dmqtt.username=$MQTT_USERNAME \
     -Dmqtt.password=$MQTT_PASSWORD \
     -Dmqtt.keepAliveInterval=$MQTT_KEEP_ALIVE_INTERVAL \
     -Dmqtt.connectionTimeout=$MQTT_CONNECTION_TIMEOUT \
     -jar /usr/local/lib/app.jar